class CreateImportHierarchyJob < ActiveRecord::Migration
  def change
    create_table :import_hierarchy_jobs do |t|
      t.string :status
      t.string :original_filename
      t.integer :user_id
      t.integer :collection_id
      t.datetime :finished_at
      t.text :exception

      t.timestamps
    end
  end
end
