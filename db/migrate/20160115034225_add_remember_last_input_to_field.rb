class AddRememberLastInputToField < ActiveRecord::Migration
  def change
    add_column :fields, :remember_last_input, :boolean, :default => false
  end
end
