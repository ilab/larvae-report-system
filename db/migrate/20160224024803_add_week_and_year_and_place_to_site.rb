class AddWeekAndYearAndPlaceToSite < ActiveRecord::Migration
  def change
  	add_column :sites, :week, :integer
  	add_column :sites, :year, :integer
  	add_column :sites, :place_id, :integer
  end
end
