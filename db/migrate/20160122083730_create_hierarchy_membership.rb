class CreateHierarchyMembership < ActiveRecord::Migration
  def change
    create_table :hierarchy_memberships do |t|
    	t.integer :user_id
      t.integer :place_id

      t.timestamps
    end
  end
end
