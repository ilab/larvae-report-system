class AddCreateToLayerMemberShip < ActiveRecord::Migration
  def change
  	add_column :layer_memberships, :create, :boolean, :default => false
  end
end
