class AddUniqueDateToSite < ActiveRecord::Migration
  def change
  	add_column :sites, :unique_date, :integer
  end
end
