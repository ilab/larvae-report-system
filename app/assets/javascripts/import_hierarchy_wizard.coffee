#= require import_hierarchy_wizard/on_import_hierarchy_wizard
#= require_tree ./import_hierarchy_wizard/.

# We do the check again so tests don't trigger this initialization
onImportHierarchyWizard -> if $('#import-hierarchy-wizard-main').length > 0

  match = window.location.toString().match(/\/collections\/(\d+)\/places\/import_hierarchy_wizard/)
  collectionId = parseInt(match[1])

  $.get "/collections/#{collectionId}/fields.json", {}, (layers) =>
	  $.get "/collections/#{collectionId}/places/import_hierarchy_wizard/places.json", {}, (places) =>
	    window.model = new HierarchyMembershipViewModel
	    window.model.initialize true, places["data"], places["error"], collectionId, layers, []
	    ko.applyBindings window.model