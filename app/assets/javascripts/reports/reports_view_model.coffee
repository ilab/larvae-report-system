class @ReportsViewModel
  initialize: (collectionId) ->
    _self = @
    @collectionId = ko.observable(collectionId)
    @selectedPlace = ko.observable()

    @listWeek = ko.observableArray()
    @listYear = ko.observableArray()
    @listMonth = ko.observableArray()
    @fields = ko.observableArray()

    @fromWeek = ko.observable()
    @fromMonth = ko.observable()
    @fromYear = ko.observable()
    @fromWeeklyReportWeek = ko.observable()
    @fromWeeklyReportYear = ko.observable()
    @fromMonthlyReportWeek = ko.observable()
    @fromMonthlyReportYear = ko.observable()


    @toWeek = ko.observable()
    @toMonth = ko.observable()
    @toYear = ko.observable()
    @toWeeklyReportWeek = ko.observable()
    @toWeeklyReportYear = ko.observable()
    @toMonthlyReportWeek = ko.observable()
    @toMonthlyReportYear = ko.observable()

    @selectedField = ko.observable()
    @selectedWeeklyField = ko.observable()

    @listWeek.push week for week in [1..52]
    @listYear.push year for year in [2015..2020]
    @listMonth.push month for month in [1..12]

    @listAvailableField = ko.computed =>
      list = $.map(@fields(), (x) -> x["id"])

  labelFor: (id) =>
    for option in @fields()
      if option.id == id
        return option["name"]
    null

  selectField: (elm)=>
    elm["checked"] = !elm["checked"]
    true

  generateReport: ()=>
    paramFields = " "
    $.map(@fields(), (x) -> 
      if x["checked"]
        paramFields = paramFields + "&field_ids[]=#{x['id']}"
    )
    console.log(paramFields)
    window.location = "/collections/#{@collectionId()}/reports/report?fromWeek=#{@fromWeek()}&fromYear=#{@fromYear()}&toWeek=#{@toWeek()}&toYear=#{@toYear()}#{paramFields}"

  generateMonthlyGraph: ()=>
    window.location = "/collections/#{@collectionId()}/reports/monthly_graph?field_id=#{@selectedField()}&fromMonth=#{@fromMonthlyReportWeek()}&fromYear=#{@fromMonthlyReportYear()}&toMonth=#{@toMonthlyReportWeek()}&toYear=#{@toMonthlyReportYear()}"

  generateWeeklyGraph: ()=>
    window.location = "/collections/#{@collectionId()}/reports/weekly_graph?field_id=#{@selectedWeeklyField()}&fromWeek=#{@fromWeeklyReportWeek()}&fromYear=#{@fromWeeklyReportYear()}&toWeek=#{@toWeeklyReportWeek()}&toYear=#{@toWeeklyReportYear()}"
