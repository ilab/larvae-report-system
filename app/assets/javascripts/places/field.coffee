
class @Field
  constructor: (data) ->
    @esCode = "#{data.id}"
    @code = data.code
    @name = data.name
    @kind = data.kind
    @is_mandatory = data.is_mandatory 

    @is_enable_field_logic = data.is_enable_field_logic

    @photo = '' 
    @preKeyCode = null
    @photoPath = '/photo_field/'
    @showInGroupBy = @kind in ['select_one', 'select_many', 'hierarchy']
    @writeable = @originalWriteable = data?.writeable

    @createable = @originalCreateable = data?.createable
    
    @allowsDecimals = ko.observable data?.config?.allows_decimals == 'true'
    @value = ko.observable()

    @value.subscribe => @setFieldFocus()

    @hasValue = ko.computed =>
      if @kind == 'yes_no'
        true
      else
        @value() && (if @kind == 'select_many' then @value().length > 0 else @value())

    @remember_last_input = ko.observable data?.remember_last_input
    @default_value = ko.observable data?.default_value

    if @kind == 'numeric'
      @keyType = if @allowsDecimals() then 'decimal' else 'integer'
      @range = if data.config?.range?.minimum? || data.config?.range?.maximum?
                data.config?.range
      @field_logics = if data.config?.field_logics?
                        $.map data.config.field_logics, (x) => new FieldLogic x
                      else
                        []

    if @kind in ['yes_no', 'select_one', 'select_many']
      @field_logics = if data.config?.field_logics?
                        $.map data.config.field_logics, (x) => new FieldLogic x
                      else
                        []


    if @kind in ['select_one', 'select_many']
      @options = if data.config?.options?
                  $.map data.config.options, (x) => new Option x
                else
                  []
      @optionsIds = $.map @options, (x) => x.id

      # Add the 'no value' option
      @optionsIds.unshift('')
      @optionsUI = [new Option {id: '', label: window.t('javascripts.collections.fields.no_value') }].concat(@options)
      @optionsUIIds = $.map @optionsUI, (x) => x.id

      @hierarchy = @options

    if @kind == 'location'
      @locations = if data.config?.locations?
                    $.map data.config.locations, (x) => new Location x
                   else
                    []
      @resultLocations = ko.observableArray([])
      @filterText = ko.observable('')
      
      @resultLocationsUI =  ko.observableArray([])
      @offset = 0 
      @limit = 3
      @showLocations = ko.observable(false)
      @remainingLocations = ko.computed =>
        remaining = if @value()
          location = @findLocationName(@value())
          @filterText(location.name) if location
          @resultLocationsUI().filter((x) => @value()?.indexOf(x.id) == -1 && x.name.toLowerCase().indexOf(@filterText().toLowerCase()) != -1)
        else
          @resultLocationsUI().filter((x) => x.name.toLowerCase().indexOf(@filterText().toLowerCase()) != -1)
        #sort the result by name
        remaining.sort (a, b) => 
          return -1 if a.name < b.name
          return 1  if a.name > b.name
          return 0          
        remaining 



      @maximumSearchLength = data.config?.maximumSearchLength

    if @kind == 'hierarchy'
      @hierarchy = data.config?.hierarchy

    @buildHierarchyItems() if @hierarchy?

    if @kind == 'select_many'
      @filter = ko.observable('') # The text for filtering options in a select_many
      @remainingOptions = ko.computed =>
        option.selected(false) for option in @options
        remaining = if @value()
          @options.filter((x) => @value()?.indexOf(x.id) == -1 && x.label.toLowerCase().indexOf(@filter().toLowerCase()) == 0)
        else
          @options.filter((x) => x.label.toLowerCase().indexOf(@filter().toLowerCase()) == 0)
        remaining[0].selected(true) if remaining.length > 0
        remaining
    else
      @filter = ->

    if @kind == 'calculation'
      @digitsPrecision = data?.config?.digits_precision
      @codeCalculation = data.config?.code_calculation
      @dependentFields = data.config?.dependent_fields