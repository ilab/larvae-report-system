class @Place
  constructor: (data) ->
    @name = ko.observable data?.name
    @code = ko.observable data?.code
    @ancestry = ko.observable data?.ancestry
    @kind_of = ko.observable data?.kind_of
