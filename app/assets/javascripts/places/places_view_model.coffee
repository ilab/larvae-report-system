class @PlacesViewModel
  initialize: (admin, places, collectionId, layers, memberships) ->
    _self = @
    @collectionId = ko.observable(collectionId)
    @selectedPlace = ko.observable()

    @hierarchy = places

    @admin = ko.observable admin

    @showRegisterNewMember = ko.observable(false)
    @showRegisterNewHierarchy = ko.observable(false)
    @showEditHierarchy = ko.observable(false)
    @email = ko.observable()
    @phoneNumber = ko.observable()
    @name = ko.observable()
    @code = ko.observable()
    @id = ko.observable()
    @smsCode = ko.observable()
    @secretCode = null
    @phoneExiste = ko.observable false
    @codeExisted = ko.observable false
    @value = ko.observable()
    @noChannelMsg = ko.observable("")
    @codeVerificationMsg = ko.observable('<p>Click "Text Me!". You will receive an SMS pin code for verification.</p>')
    @members = ko.observableArray()
    @fieldHierarchyItemsMap = {}
    @layers = ko.observableArray $.map(layers, (x) -> new Layer(x))
    @emailError = ko.computed =>
      if @hasEmail()
        atPos = @email().indexOf('@')
        dotPos = @email().indexOf('.')
        if atPos<1 || dotPos < atPos + 2 || dotPos + 2 >= @email().length
          window.t('javascripts.collections.members.errors.email_is_invalid')
        else
          null
    @phoneError = ko.computed => 
      if @hasPhone()
        if @phoneExiste()
          window.t('javascripts.collections.members.errors.phone_number_is_taken')
        else 
          null
      else
        window.t('javascripts.collections.members.errors.phone_number_is_required')#"Phone number is required"

    @nameError = ko.computed =>
      unless @hasName()
        window.t('javascripts.places.errors.name_is_invalid')

    @codeError = ko.computed => 
      if @hasCode()
        if @codeExisted()
          window.t('javascripts.places.errors.code_is_taken')
        else 
          null
      else
        window.t('javascripts.places.errors.code_is_required')#"Phone number is required"

    @hasError = ko.computed =>
      return true if @phoneError() || @emailError() || @smsCodeExiste()

    @hasHierarachyError = ko.computed =>
      return true if @nameError() || @codeError()

    @current_members = ko.observableArray $.map(memberships, (x) -> new MembershipHierarchy(_self, x))
    @fieldHierarchyItems = ko.observableArray $.map(@hierarchy, (x) => new FieldHierarchyItem(@, x))
    @currentfieldHierarchyItem = ko.observable()

  phoneNumberExist: () =>
    if (@phoneNumber())
      false
    else
      true

  smsCodeExiste: () => @smsCode() != @secretCode

  sentCodeMsg: () =>
    _self = this;
    if (@phoneNumber())
      $.post "/collections/#{ @collectionId() }/send_new_member_sms.json", phone_number: @phoneNumber(), (data) ->
        if data.errors
          _self.codeVerificationMsg(data.errors)
        else if data.status == "no_channel"
          _self.noChannelMsg("There is no SMS Gateway.")
        else if data.status == "channel_disconnected"
          _self.noChannelMsg("The channel is disconnected.")
        else
          _self.secretCode = data.secret_code
          _self.codeVerificationMsg('<p style="color: green;">The pin code has been sent to the phone number above. Please enter the pin code in the textbox for verification.</p>')


  showRegisterMembership: () =>
    @showRegisterNewMember(true)

  editRegisterHierarchy: () =>
    @showEditHierarchy(true)
    @name(@currentfieldHierarchyItem().name())
    @code(@currentfieldHierarchyItem().code())
    @id(@currentfieldHierarchyItem().id)


  showRegisterHierarchy: () =>
    @showRegisterNewHierarchy(true)

  hideRegisterHierarchy: () =>
    @showRegisterNewHierarchy(false)

  hideEditHierarchy: () =>
    @showEditHierarchy(false)
    @name("")
    @code("")
    @id("")

  hideRegisterMembership: () =>
    @showRegisterNewMember(false)
    @smsCode("")
    @secretCode = null
    @phoneNumber("")
    @email("")
    @codeVerificationMsg('Click "Text Me!". You will receive an SMS pin code for verification.')
    @noChannelMsg("")

  hasEmail: => $.trim(@email()).length > 0

  hasName: => $.trim(@name()).length > 0

  hasCode: => $.trim(@code()).length > 0

  hasPhone: => $.trim(@phoneNumber()).length > 0

  fetchNewMember: ()=>
    $.get "/collections/#{@collectionId()}/places/memberships.json",code: @value(), (memberships) =>
      @applyCurrentMembers(memberships)

  applyCurrentMembers: (members) =>
    @current_members([])
    _self = @
    $.map(members, (x) => @current_members.push(new MembershipHierarchy(_self, x)))

  createHierarchy: =>
    $.post "/collections/#{@collectionId()}/places/create_hierarchy.json", {code: @code(), parent: @value(), name: @name()}, (data) =>
      parent = model.currentfieldHierarchyItem()
      level = model.currentfieldHierarchyItem().level + 1
      newNode = new FieldHierarchyItem(@, data, parent, level)
      @currentfieldHierarchyItem().fieldHierarchyItems.push newNode
      #refresh nod in hierarchy
      @currentfieldHierarchyItem().expanded(false)
      @currentfieldHierarchyItem().expanded(true)
      # @currentfieldHierarchyItem(newNode)
      @showRegisterNewHierarchy(false)
      @name("")
      @code("")

  updateHierarchy: =>
    $.post "/collections/#{@collectionId()}/places/update_hierarchy.json", {id: @id(), code: @code(), parent: @value(), _method: 'put', name: @name()}, (data) =>
      @currentfieldHierarchyItem().name(@name())
      parent = model.currentfieldHierarchyItem()
      if parent != null
          parent.expanded(false)
          parent.expanded(true)
      else
        @fieldHierarchyItems.destroy @currentfieldHierarchyItem()
      @showEditHierarchy(false)
      @name("")
      @code("")
      @id("")

  deleteHierarchy: =>
    if confirm("Are you sure to delete?")
      $.post "/collections/#{@collectionId()}/places/#{@value()}.json", {_method: 'delete'}, (data) =>
        parent = @currentfieldHierarchyItem().parent
        if parent != null
          index = parent.fieldHierarchyItems.indexOf @currentfieldHierarchyItem()
          if index > -1
            parent.fieldHierarchyItems.splice(index, 1)
            parent.expanded(false)
            parent.expanded(true)
        else
          @fieldHierarchyItems.destroy @currentfieldHierarchyItem()





