@initPlaces = (userId, collectionId, admin, layers) ->
  window.userId = userId

  $.get "/collections/#{collectionId}/places/my_supervision_place.json", (places) ->
    window.model = new PlacesViewModel
    window.model.initialize admin, places, collectionId, layers, []
    ko.applyBindings window.model

    $member_email = $('#member_email')
    $hierarchy_name = $('#nameHierarchy')
    $hierarchy_code = $('#codeHierarchy')

    createMembership = (email = $member_email.val()) ->
      if $.trim(email).length > 0
        $.post "/collections/#{collectionId}/places/create_memberships.json", {email: email, place_id: window.model.value()}, (data) ->
          if data.status == 'added'
            new_member = new MembershipHierarchy(window.model, { user_id: data.user_id, user_display_name: data.user_display_name })
            window.model.current_members.push new_member
            $member_email.val('')

    $member_email.autocomplete
      source: (term, callback) ->
        $.ajax "/collections/#{collectionId}/memberships/invitable.json?#{$.param term}",
          success: (data) ->
            if data.length == 0
              callback(['No users found'])
              $('a', $member_email.autocomplete('widget')).attr('style', 'color: red')
            else
              callback(data)
      select: (event, ui) ->
        if(ui.item.label == 'No users found')
          event.preventDefault()
        else
          createMembership(ui.item.label)
      appendTo: '#autocomplete_container'
