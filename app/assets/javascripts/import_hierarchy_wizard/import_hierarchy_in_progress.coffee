#= require import_hierarchy_wizard/on_import_hierarchy_wizard
#= require_tree .

# We do the check again so tests don't trigger this initialization
onImportHierarchyInProgress -> if $('#import-hierarchy-in-progress').length > 0
  match = window.location.toString().match(/\/collections\/(\d+)\/places\/import_hierarchy_wizard/)
  collectionId = parseInt(match[1])

  poll_status = ->
    $.get "/collections/#{collectionId}/places/import_hierarchy_wizard/job_status.json", {}, (data) =>
      status = data.status
      if status == 'finished' || status == 'failed'
        window.location = "/collections/#{collectionId}/places/import_hierarchy_wizard/import_#{status}"
      else if status == 'in_progress'
        $(".pending_jobs").hide()
        $(".in_progress").show()

      setTimeout(poll_status, 2000)

  poll_status()
