window.onImportHierarchyWizard ?= (callback) -> $(-> callback() if $('#import-hierarchy-wizard-main').length > 0)

window.onImportHierarchyInProgress ?= (callback) -> $(-> callback() if $('#import-hierarchy-in-progress').length > 0)