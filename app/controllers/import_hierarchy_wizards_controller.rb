class ImportHierarchyWizardsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :show_collection_breadcrumb
  before_filter :show_properties_breadcrumb
  before_filter :authenticate_collection_admin!, only: :logs

  expose(:import_hierarchy_job) { ImportHierarchyJob.last_for current_user, collection }
  expose(:failed_import_hierarchy_jobs) { ImportHierarchyJob.where(collection_id: collection.id).where(status: 'failed').order('id desc').page(params[:page]).per_page(10) }

  def index
    return redirect_to import_in_progress_collection_import_hierarchy_wizard_path(collection) if (import_hierarchy_job && (import_hierarchy_job.status_pending? || import_hierarchy_job.status_in_progress?))

    add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
    add_breadcrumb I18n.t('views.collections.tab.import_wizard'), collection_import_hierarchy_wizard_path(collection)
  end

  def upload_xls
    begin
      ImportHierarchyWizard.import current_user, collection, params[:file].original_filename, params[:file].read
      redirect_to adjustments_collection_import_hierarchy_wizard_path(collection)
    rescue => ex
      redirect_to collection_import_hierarchy_wizard_path(collection), :alert => ex.message
    end
  end

  def guess_columns_spec
    render json: ImportHierarchyWizard.guess_columns_spec(current_user, collection), :root =>false
  end

  def adjustments
    add_breadcrumb I18n.t('views.collections.tab.import_wizard'), collection_import_hierarchy_wizard_path(collection)
  end

  def validate_sites_with_columns
    render json: ImportHierarchyWizard.validate_sites_with_columns(current_user, collection, JSON.parse(params[:columns]))
  end

  def execute
    ImportHierarchyWizard.enqueue_job current_user, collection
    render json: :ok
  end

  def import_in_progress
    redirect_to import_finished_collection_import_hierarchy_wizard_path(collection) if import_hierarchy_job.status_finished?

    add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
    add_breadcrumb I18n.t('views.collections.tab.import_wizard'), collection_import_hierarchy_wizard_path(collection)
  end

  def import_finished
    add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
    add_breadcrumb I18n.t('views.collections.tab.import_wizard'), collection_import_hierarchy_wizard_path(collection)
  end

  def import_failed
    add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
    add_breadcrumb I18n.t('views.collections.tab.import_wizard'), collection_import_hierarchy_wizard_path(collection)
  end

  def cancel_pending_jobs
    ImportHierarchyWizard.cancel_pending_jobs(current_user, collection)
    flash[:notice] = I18n.t('views.import_wizards.import_canceled')
    redirect_to collection_import_hierarchy_wizard_path
  end

  def job_status
    render json: {:status => import_hierarchy_job.status}
  end

  def logs
    add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
    add_breadcrumb I18n.t('views.collections.tab.import_wizard'), collection_import_hierarchy_wizard_path(collection)
  end

  def places
    file_name = ImportHierarchyWizard.get_file_path(@current_user, collection)
    csv_string = ImportHierarchyWizard.read_place(file_name)
    hierarchy = collection.decode_hierarchy_csv(csv_string)
    hierarchy_errors = ImportHierarchyWizard.generate_error_description_list(hierarchy)
    errors = ImportHierarchyWizard.validate_existing(csv_string)
    render :json => {data: hierarchy, error: hierarchy_errors}, :root => false
  end

  def memberships
    file_name = ImportHierarchyWizard.get_file_path(@current_user, collection)
    memberships = ImportHierarchyWizard.get_memberships(params[:code], file_name)
    render :json => memberships.to_json, :root => false
  end

end
