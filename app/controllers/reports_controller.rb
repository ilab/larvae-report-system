class ReportsController < ApplicationController
  before_filter :setup_guest_user, :if => Proc.new { collection }
  before_filter :authenticate_user!
  before_filter :authenticate_collection_admin!, :only => [:download, :index]
  # authorize_resource :except => [:render_breadcrumbs], :decent_exposure => true, :id_param => :collection_id

  def index
    @places = Place.to_hierachy(collection)
    @fields = collection.fields.select([:name, :id, :kind]).to_json
    respond_to do |format|
      format.html do
        show_collection_breadcrumb
        add_breadcrumb I18n.t('views.collections.index.properties'), collection_path(collection)
        add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
      end
      format.json { render json:  @places, :root => false}
    end  
  end

  # def download
  #   ods = Place.find_all_by_kind_of("OD")
  #   from_week = params[:fromWeek]
  #   from_year = params[:fromYear]
  #   to_week = params[:toWeek]
  #   to_year = params[:toYear]
  #   from = from_year.to_i * 52 + from_week.to_i
  #   to = to_year.to_i * 52 + to_week.to_i
  #   @data = {}
  #   @total = {}
  #   ods.each do |od|
  #     sites = od.get_sites(current_user, from, to)
  #     @data.merge!(sites)
  #     sites.each do |key, value|
  #       @total.merge!(value) {|key,val1,val2| val1.to_i + val2.to_i}
  #     end
  #   end
  #   fields = collection.fields.in_groups(2)
  #   @fields_first = fields[0]
  #   @fields_second = fields[1]
  #   render pdf: "report_#{from_week}_#{from_year}_to#{to_week}_#{to_year}.pdf", :layout => false, orientation: 'Landscape'
  # end

  def report
    ods = Place.find_all_by_kind_of("OD")
    calculation_fields = collection.fields.find_all_by_kind("calculation")
    @field_ids = params["field_ids"]
    @from_week = params[:fromWeek]
    @from_year = params[:fromYear]
    @to_week = params[:toWeek]
    @to_year = params[:toYear]
    from = @from_year.to_i * 52 + @from_week.to_i
    to = @to_year.to_i * 52 + @to_week.to_i
    @data = {}
    @data_village = {}
    @total = {}
    @total_village = {}
    ods.each do |od|
      @data_village = {}
      @total_village = {}
      villages = od.children
      villages.each do |v|
        sites = v.get_sites(current_user, from, to)
        @data_village.merge!(sites)
        sites.each do |key, value|
          @total_village.merge!(value) {|key,val1,val2| val1.to_i + val2.to_i}
        end

        calculation_fields.each do |field|
          field.config["dependent_fields"].each do |key, df|
            field.config["code_calculation"].gsub!("${#{df['code']}}", '"' +  @total_village["#{df['id']}"].to_s + '"' + ".to_f")
          end
          p field.config["code_calculation"]
          @total_village[field.id.to_s] = eval(field.config["code_calculation"]).round(2)
          @total_village[field.id.to_s] = 0 if @total_village[field.id.to_s].nan?
        end
      end
      @total.merge!({"#{od.id}" => @total_village})
      @data.merge!({"#{od.id}" => @data_village})

    end
    @fields = collection.fields.where("id IN (?)",@field_ids)

    # render :layout => false
    render pdf: "report_#{@from_week}_#{@from_year}_to_#{@to_week}_#{@to_year}.pdf", :layout => false
  end

  def monthly_graph
    @from_month = params[:fromMonth]
    @from_year = params[:fromYear]
    @to_month = params[:toMonth]
    @to_year = params[:toYear]
    @field_id = params[:field_id]
    # render pdf: "report.pdf", :layout => false
    render :layout => false
  end

  def get_monthly_graph
    #TODO Replace with selected year
    column = collection.places.find_all_by_kind_of("OD").map(&:name)
    districts = collection.places.find_all_by_kind_of("OD")
    from_month = params[:from_month]
    from_year = params[:from_year]
    to_month = params[:to_month]
    to_year = params[:to_year]
    field_id = params[:field_id]
    diff = (to_year.to_i - from_year.to_i).abs * 12 + (to_month.to_i - from_month.to_i).abs + 1

    now = DateTime.now()
    from_date = now.change(:year => from_year.to_i, :month => from_month.to_i.to_i)
    to_date = now.change(:year => to_year.to_i, :month => to_month.to_i)
    if from_date < to_date
      base_date = from_date.beginning_of_month 
    else
      base_date = to_date.beginning_of_month 
    end
    data = []
    tmp = []
    (1..diff).each do |m|
      tmp = [m]
      districts.each do |d|
        from_date = base_date
        to_date = from_date.at_end_of_month
        sites = d.get_site_by_date(current_user, from_date, to_date)
        #Replace with Field id
        if sites[field_id]
          tmp.push(sites[field_id])
        else
          tmp.push(0)
        end
      end
      base_date = base_date.next_month
      data.push(tmp)
    end
    render :json => {:column => column, :data => data}.to_json, :root => false, :head => :ok
  end

  def weekly_graph
    @from_week = params[:fromWeek]
    @from_year = params[:fromYear]
    @to_week = params[:toWeek]
    @to_year = params[:toYear]
    @field_id = params[:field_id]
    # render pdf: "report.pdf", :layout => false
    render :layout => false
  end

  def get_weekly_graph
    #TODO Replace with selected year
    column = collection.places.find_all_by_kind_of("OD").map(&:name)
    districts = collection.places.find_all_by_kind_of("OD")
    from_week = params[:from_week]
    from_year = params[:from_year]
    to_week = params[:to_week]
    to_year = params[:to_year]
    from = from_year.to_i * 52 + from_week.to_i
    to = to_year.to_i * 52 + to_week.to_i
    field_id = params[:field_id]
    diff = (to - from).abs + 1
    if from < to
      base_week = from
    else
      base_week = to
    end
    data = []
    tmp = []
    (1..diff).each do |m|
      tmp = [m]
      districts.each do |d|
        sites = d.get_sites(current_user, base_week, base_week)
        #Replace with Field id
        if sites[d.id.to_s][field_id]
          tmp.push(sites[d.id.to_s][field_id])
        else
          tmp.push(0)
        end
      end
      base_week = base_week + 1
      data.push(tmp)
    end
    render :json => {:column => column, :data => data}.to_json, :root => false, :head => :ok
  end

end