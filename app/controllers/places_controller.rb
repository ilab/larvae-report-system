
require "spreadsheet"

class PlacesController < ApplicationController
  before_filter :setup_guest_user, :if => Proc.new { collection }
  before_filter :authenticate_user_management!, :only => [:create, :index, :create_memberships, :delete_membership, :create_hierarchy, :create_hierarchy, :index, :destroy]
  # before_filter :authenticate_collection_admin! , :only => [:create, :index, :create_memberships, :delete_membership, :create_hierarchy, :create_hierarchy, :index, :destroy]
  # authorize_resource :except => [:render_breadcrumbs], :decent_exposure => true, :id_param => :collection_id

  expose(:collections){
    if current_user && !current_user.is_guest
      # public collections are accesible by all users
      # here we only need the ones in which current_user is a member
      current_user.collections
    else
      Collection.all
    end
  }

  expose(:import_hierarchy_job) { ImportHierarchyJob.last_for current_user, collection }
  expose(:failed_import_jobs) { ImportHierarchyJob.where(collection_id: collection.id).where(status: 'failed').order('id desc').page(params[:page]).per_page(10) }

  include ApplicationHelper

  def index
    @places = Place.to_hierachy(collection)
    respond_to do |format|
      format.html do
        show_collection_breadcrumb
        add_breadcrumb I18n.t('views.collections.index.properties'), collection_path(collection)
        add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
      end
      format.json { render json:  @places, :root => false}
    end
  end

  def destroy
    @place = Place.find(params[:id])
    begin
      @place.destroy!
      render :json => @place.to_json
    rescue ActiveRecord::StatementInvalid => error
      redirect_to places_with_ref_path(@place.id), alert: 'Failed to remove place. Make sure there are no users associate to this place'
    end

  end

  def create_memberships
    user = User.find_by_email params[:email]
    if user && !user.memberships.where(:collection_id => collection.id).exists?
      user.memberships.create! :collection_id => collection.id
      user.register_to_place params[:place_id]
      render json: {status: :added, user_id: user.id, user_display_name: user.display_name}
    else
      register_new_member
      #render json: {status: :not_added}
    end
  end

  def create_hierarchy
    parent = Place.find(params[:parent])
    name = params[:name]
    code = params[:code]
    place = Place.create!(:name => name, :code => code, :parent_id => parent.id, :collection_id => params[:collection_id])
    render json: place.to_json
  end

  def update_hierarchy
    place = Place.find(params[:id])
    name = params[:name]
    code = params[:code]
    place.update_attributes!(:name => name, :code => code)
    render json: place.to_json
  end

  def register_new_member
    if (params[:user][:email].strip.length == 0)
      params[:user][:email] = User.generate_default_email
    end

    params[:user][:password] = User.generate_random_password if params[:user]
    if (User.find_all_by_phone_number(params[:user][:phone_number]).count == 0)
      user = User.create params[:user] if params[:user]
      user.confirmed_at = Time.now
      if user.save!
        user = User.find_by_email params[:user][:email]
        user.memberships.create! admin: false, user_id: user.id, collection_id: collection.id
        membership = collection.memberships.find_by_user_id user.id
        user_display_name = User.generate_user_display_name user
        if membership
          collection.layers.each do |l|
            membership.set_layer_access :access => true, :layer_id => l.id, :verb => "read"
          end
        end
        layer_memberships = collection.layer_memberships.all.inject({}) do |hash, membership|
          (hash[membership.user_id] ||= []) << membership
          hash
        end
        render json: {
                      status: :ok,
                      user_id: user.id,
                      layers: (layer_memberships[membership.user_id] || []).map{|x| {layer_id: x.layer_id, read: x.read?, write: x.write?}},
                      user_display_name: user_display_name
                      }
      else
        render json: :unsaved
      end
    else
      render json: {status: :phone_existed}
    end
  end

  def memberships
    place = Place.find_by_id params[:code]
    user_list = HierarchyMembership.find_all_by_place_id(place.id).map(&:user_id) || []
    layer_memberships = collection.layer_memberships.all.inject({}) do |hash, membership|
      (hash[membership.user_id] ||= []) << membership
      hash
    end
    unless user_list.empty?
      list_members = collection.memberships.where("user_id in (?)", user_list ).includes([:user, :read_sites_permission, :write_sites_permission])
      memberships = list_members.all.map do |membership|
        user_display_name = User.generate_user_display_name membership.user
        if user_display_name == membership.user.phone_number
          user_phone_number = ""
        else
          user_phone_number = membership.user.phone_number
        end

        {
          user_id: membership.user_id,
          user_display_name: user_display_name,
          user_phone_number: user_phone_number,
          admin: membership.admin?,
          layers: (layer_memberships[membership.user_id] || []).map{|x| {layer_id: x.layer_id, read: x.read?, write: x.write?, create: x.create?}},
          sites: {
            none: membership.none_sites_permission,
            read: membership.read_sites_permission,
            write: membership.write_sites_permission,
            create: membership.create_sites_permission
          }
        }
      end
    else
      memberships = []
    end
    render json: memberships, :root => false
  end

  def delete_membership
    membership = collection.memberships.find_by_user_id params[:id]
    hierarchy_member = HierarchyMembership.find_by_user_id_and_place_id(params[:id], params[:place_id])
    if leave_collection_permission? || membership.user_id != current_user.id
      membership.destroy
      hierarchy_member.destroy! if hierarchy_member
    end
    render json: {membership: membership, hierarchy_member: hierarchy_member}, :root => false
  end

  def download_template
    file_name = "template-#{collection.id}.xls"
    file = "#{Rails.root}/public/#{file_name}"

    @workbook = Spreadsheet::Workbook.new

    build_template_header "Hierarchy", 0, ["Code","Parent Code","Name"]
    build_template_header "User", 1, ["Email","Password","Password Confirmation", "Phone Number", "Place code"]

    @workbook.write file

    send_file file, content_type: "application/vnd.ms-excel"
  end

  def download
    file_name = "hierarchy-#{collection.id}.xls"
    file = "#{Rails.root}/public/#{file_name}"

    @workbook = Spreadsheet::Workbook.new

    build_template_header "Hierarchy", 0, ["Code","Parent Code","Name"]
    collection.places.each do |place|
      parent = place.parent
      if parent
        parent_code = parent.code
      else
        nil
      end
      build_data(0,[place.code,parent_code,place.name])
    end

    build_template_header "User", 1, ["Email","Password","Password Confirmation", "Phone Number", "Place code"]

    collection.memberships.each do |member|
      user = member.user
      hm = HierarchyMembership.find_by_user_id(user.id)
      if hm
        place = hm.place
        place_code = place.code
      else
        place_code = nil
      end
      build_data(1,[user.email, "*********", "*********", user.phone_number, place_code]) unless place_code.nil?
    end

    @workbook.write file

    send_file file, content_type: "application/vnd.ms-excel"
  end

  def build_template_header name, sheet_number, header
    @workbook.create_worksheet :name => name
    @workbook.worksheet(sheet_number).insert_row(0, header)
  end

  def build_data sheet_number, data
    @workbook.worksheet(sheet_number).insert_row(@workbook.worksheet(sheet_number).rows.size, data)
  end

  def import_wizard
    respond_to do |format|
      format.html do
        show_collection_breadcrumb
        add_breadcrumb I18n.t('views.collections.index.properties'), collection_path(collection)
        add_breadcrumb I18n.t('views.collections.tab.health_structure'), collection_places_path(collection)
        add_breadcrumb I18n.t('views.collections.tab.import_wizard'), collection_path(collection)
      end
    end
  end

  def upload_xls
    begin
      ImportHierarchyWizard.import current_user, collection, params[:file].original_filename, params[:file].read
      redirect_to adjustments_collection_import_wizard_path(collection)
    rescue => ex
      redirect_to import_wizard_collection_places_path(collection), :alert => ex.message
    end
  end

  def my_supervision_place
    if current_user.admins? collection
      places = collection.places.pluck(:id, :code , :ancestry, :name, :kind_of)
      data = Place.to_hierarchy_format places
      render :json => data, :root => false
    else
      place = HierarchyMembership.find_by_user_id(current_user.id).place
      render :json => place.get_hierarchy_view_format , :root => false
    end
  end
end
