module Api::V1

  class PlacesController < ApplicationController
    include Concerns::CheckApiDocs
    before_filter :authenticate_api_user!
    skip_before_filter :verify_authenticity_token

    def my_supervision_place
      unless current_user.admins? collection
        place = HierarchyMembership.find_by_user_id(current_user.id).place
        render :json => place.children.to_json
      else
        render :json => collection.places.to_json
      end
    end

    def get_parent_place_by_ancestry_id
      if params[:ancestry]
        ancestry_id = params[:ancestry].split("/").last
        place = Place.find(ancestry_id)
        render :json => place.to_json
      else
        render :json => nil
      end
    end

    def memberships
      membership = nil
      if current_user
        layer_memberships = collection.layer_memberships.all.inject({}) do |hash, membership|
          (hash[membership.user_id] ||= []) << membership
          hash
        end
        members = collection.memberships.where("user_id" => current_user.id ).includes([:user, :read_sites_permission, :write_sites_permission])
        membership = members[0]
        user_display_name = User.generate_user_display_name membership.user
        if user_display_name == membership.user.phone_number
          user_phone_number = ""
        else
          user_phone_number = membership.user.phone_number
        end

        membership = {
          user_id: membership.user_id,
          user_display_name: user_display_name,
          user_phone_number: user_phone_number,
          admin: membership.admin?,
          layers: (layer_memberships[membership.user_id] || []).map{|x| {layer_id: x.layer_id, read: x.read?, write: x.write?, create: x.create?}},
          sites: {
            none: membership.none_sites_permission,
            read: membership.read_sites_permission,
            write: membership.write_sites_permission,
            create: membership.create_sites_permission
          }
        }
      end
      render json: membership, :root => false
    end
  end
end
