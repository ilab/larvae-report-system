module Api::V1
  class FieldsController < ApplicationController
    include Concerns::CheckApiDocs

    before_filter :authenticate_api_user!
	  skip_before_filter :verify_authenticity_token

    def index
    	options = {}
    	if params[:place_id]
	  		options[:place_id] = params[:place_id]
	  	end
      fields = collection.visible_layers_for(current_user, options)
      render json: fields, :root => false
    end
  end
end