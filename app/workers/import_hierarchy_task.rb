class ImportHierarchyTask
  @queue = :import_queue

  def self.perform user_id, collection_id
  	begin
       ImportHierarchyWizard.execute(user_id, collection_id)
     rescue Exception => ex
       (ImportHierarchyJob.last_for user_id, collection_id).failed(ex)
     end
   end 
end
