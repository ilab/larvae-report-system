
require "spreadsheet" 

class ImportHierarchyWizard
  TmpDir = "#{Rails.root}/tmp/import_hierarcy_wizard"

  class << self
    def enqueue_job(user, collection)
      mark_job_as_pending user, collection

      # Enqueue job with user_id, collection_id
      Resque.enqueue ImportHierarchyTask, user.id, collection.id
    end

    def cancel_pending_jobs(user, collection)
      mark_job_as_canceled_by_user(user, collection)
      delete_file(user, collection)
    end

    def import(user, collection, original_filename, contents)
      # Store representation of import job in database to enable status tracking later
      ImportHierarchyJob.uploaded original_filename, user, collection

      FileUtils.mkdir_p TmpDir

      raise "Invalid file format. Only XLS files are allowed." unless File.extname(original_filename) == '.xls'

      begin
        File.open(file_for(user, collection), "wb") { |file| file << contents }
        # xls = read_xls_for(user, collection)
      rescue CSV::MalformedCSVError => ex
        raise "The file is not a valid XLS: #{ex.message}"
      end
    end

    def add_new_hierarchy_error(csv_column_number, hierarchy_errors)
      if hierarchy_errors.length >0 && hierarchy_errors[0][:new_hierarchy_columns].length >0
        hierarchy_errors[0][:new_hierarchy_columns] << csv_column_number
      else
        hierarchy_errors = [{:new_hierarchy_columns => [csv_column_number]}]
      end
      hierarchy_errors
    end

    def get_sites(csv, user, collection, columns_spec, page)
      csv_columns = csv[1 .. 10]
      processed_csv_columns = []
      csv_columns.each do |csv_column|
        processed_csv_columns << csv_column.map{|csv_field_value| {value: csv_field_value} }
      end
      processed_csv_columns
    end

    def execute(user, collection)
      #Execute may be called with actual user and collection entities, or their ids.
      if !(user.is_a?(User) && collection.is_a?(Collection))
        #If the method's been called with ids instead of entities
        user = User.find(user)
        collection = Collection.find(collection)
      end

      import_hierarchy_job = ImportHierarchyJob.last_for user, collection

      # Execution should continue only if the job is in status pending (user may canceled it)
      if import_hierarchy_job.status == 'pending'
        mark_job_as_in_progress(user, collection)
        execute_with_entities(user, collection)
      end
    end

    def execute_with_entities(user, collection)
      begin
        file_name = get_file_path(user, collection)
        data =  Spreadsheet.open file_name
        rows = read_sheet(data,0)
        rows.shift
        import_place(rows, collection)

        rows = read_sheet(data,1)
        rows.shift
        import_user(rows, collection)

        mark_job_as_finished(user, collection)

      rescue Exception => ex
        raise ex
      end

      delete_file(user, collection)
    end

    def read_sheet spreadsheet, index
      sheet = spreadsheet.worksheet index
      return sheet.rows
    end

    def import_place rows, collection
      rows.each do |r|
        row = r.to_a
        code = (row[0].nil? or row[0].to_i == 0 or row[0] == "")? " ": row[0].to_i
        parent_code = (row[1].nil? or row[1].to_i == 0 or row[1].to_s.strip == "")? nil: row[1].to_i
        unless parent_code.nil?
          parent = Place.find_by_code(parent_code)
          if parent.nil?
            parent = find_and_create_parent(rows, parent_code)
          end
          parent_id = parent.id
        else
          parent_id = nil
        end
        Place.create!(:name => row[2], :code => code, :parent_id => parent_id, :collection_id => collection.id) unless Place.find_by_code(code)
      end
    end

    def import_user rows, collection
      rows.each do |r|
        row = r.to_a
        email = row[0]
        password = row[1]
        password_confirmation = row[2]
        phone_number = row[3]
        place_code = (row[4].nil? or row[4].to_i == 0 or row[4].to_s.strip == "")? nil: row[4].to_i
        place = Place.find_by_code(place_code)
        user = User.find_by_email(email)
        unless user
          user = User.create!(:email => email, :password => password, :password_confirmation => password_confirmation, :confirmed_at => DateTime.now(), :phone_number => phone_number)
        end
        if user && !user.memberships.where(:collection_id => collection.id).exists?
          user.memberships.create! :collection_id => collection.id
        end
        user.register_to_place place.id
      end
    end

    def find_and_create_parent rows, pcode
      place = nil
      rows.each do |r|
        row = r.to_a
        code = (row[0].nil? or row[0].to_i == 0 or row[0] == "")? nil: row[0].to_i
        parent_code = (row[1].nil? or row[1].to_i == 0 or row[1].to_s.strip == "")? nil: row[1].to_i
        if code.to_s == pcode.to_s
          parent = Place.find_by_code(parent_code)
          if parent.nil?
            place = find_and_create_parent(rows, parent_code)
          else
            place = Place.create!(:name => row[1], :code => code, :parent_id => parent.id, :collection_id => collection.id)
          end
          break
        end
      end
      return place
    end

    def delete_file(user, collection)
      File.delete(file_for(user, collection))
    end

    def mark_job_as_pending(user, collection)
      # Move the corresponding ImportJob to status pending, since it'll be enqueued
      (ImportHierarchyJob.last_for user, collection).pending
    end

    def mark_job_as_canceled_by_user(user, collection)
      (ImportHierarchyJob.last_for user, collection).canceled_by_user
    end

    def mark_job_as_in_progress(user, collection)
      (ImportHierarchyJob.last_for user, collection).in_progress
    end

    def mark_job_as_finished(user, collection)
      (ImportHierarchyJob.last_for user, collection).finish
    end

    def read_place(file_name)
      data =  Spreadsheet.open file_name
      list = []
      sheet_places = data.worksheet 0
      rows = sheet_places.rows
      rows.shift
      rows.each do |r|
        row = r.to_a
        code = (row[0].nil? or row[0].to_i == 0 or row[0] == "")? " ": row[0].to_i
        parent = (row[1].nil? or row[1].to_i == 0 or row[1] == "")? " ": row[1].to_i
        list.push("#{code},#{parent},#{row[2]}")
      end
      list.join("\n")
    end

    def get_memberships(place_code, file_name)
      p place_code
      data =  Spreadsheet.open file_name
      list = []
      sheet_places = data.worksheet 1
      rows = sheet_places.rows
      rows.shift
      rows.each do |r|
        row = r.to_a
        code = (row[4].nil? or row[4].to_i == 0 or row[4] == "")? " ": row[4].to_i
        if code.to_s == place_code.to_s
          list.push({layers: [], user_id: nil, user_display_name: row[0],user_phone_number: row[3],admin: false, sites: {none: nil, read: nil, write: nil}})
        end
      end
      list
    end

    private

    def calculate_non_existent_site_id(valid_site_ids, csv_column, resmap_id_column_index)
      invalid_ids = []
      csv_column.each_with_index do |csv_field_value, field_number|
        invalid_ids << field_number unless (csv_field_value.blank? || valid_site_ids.include?(csv_field_value.to_s))
      end
      [{rows: invalid_ids, column: resmap_id_column_index}] if invalid_ids.length >0
    end

    def validate_column(user, collection, column_spec, fields, csv_column, column_number)
      if column_spec[:use_as].to_sym == :existing_field
        field = fields.detect{|e| e.id.to_s == column_spec[:field_id].to_s}
      else
        field = Field.new kind: column_spec[:kind].to_s
      end

      validated_csv_column = []
      csv_column.each_with_index do |csv_field_value, field_number|
        begin
          validate_column_value(column_spec, csv_field_value, field, collection)
        rescue => ex
          description = error_description_for_type(field, column_spec, ex)
          validated_csv_column << {description: description, row: field_number}
        end
      end

      validated_columns_grouped = validated_csv_column.group_by{|e| e[:description]}
      validated_columns_grouped.map do |description, hash|
        {description: description, column: column_number, rows: hash.map { |e| e[:row] }, type: field.value_type_description, example: field.value_hint }
      end
    end

    def error_description_for_type(field, column_spec, ex)
      column_index = column_spec[:index]
      "Some of the values in column #{column_index + 1} #{field.error_description_for_invalid_values(ex)}."
    end

    def calculate_duplicated(selection_block, groping_field)
      spec_to_validate = selection_block.call()
      spec_by_field = spec_to_validate.group_by{ |s| s[groping_field]}
      duplicated_columns = {}
      spec_by_field.each do |column_spec|
        if column_spec[1].length > 1
          duplicated_columns[column_spec[0]] = column_spec[1].map{|spec| spec[:index] }
        end
      end
      duplicated_columns
    end

    def calculate_reserved_code(selection_block)
      spec_to_validate = selection_block.call()
      invalid_columns = {}
      spec_to_validate.each do |column_spec|
        if Field.reserved_codes().include?(column_spec[:code])
          if invalid_columns[column_spec[:code]]
            invalid_columns[column_spec[:code]] << column_spec[:index]
          else
            invalid_columns[column_spec[:code]] = [column_spec[:index]]
          end
        end
      end
      invalid_columns
    end

    def calculate_missing(selection_block, missing_value)
      spec_to_validate = selection_block.call()
      missing_value_columns = []
      spec_to_validate.each do |column_spec|
        if column_spec[missing_value].blank?
          if missing_value_columns.length >0
            missing_value_columns << column_spec[:index]
          else
            missing_value_columns = [column_spec[:index]]
          end
        end
      end
      {:columns => missing_value_columns} if missing_value_columns.length >0
    end

    def calculate_existing(columns_spec, collection_fields, grouping_field)
      spec_to_validate = columns_spec.select {|spec| spec[:use_as] == 'new_field'}
      existing_columns = {}
      spec_to_validate.each do |column_spec|
        #Refactor this
        if grouping_field == 'code'
          found = collection_fields.detect{|f| f.code == column_spec[grouping_field]}
        elsif grouping_field == 'label'
          found = collection_fields.detect{|f| f.name == column_spec[grouping_field]}
        end
        if found
          if existing_columns[column_spec[grouping_field]]
            existing_columns[column_spec[grouping_field]] << column_spec[:index]
          else
            existing_columns[column_spec[grouping_field]] = [column_spec[:index]]
          end
        end
      end
      existing_columns
    end

    def validate_column_value(column_spec, field_value, field, collection)
      if field.new_record?
        validate_format_value(column_spec, field_value, collection)
      else
        field.apply_format_and_validate(field_value, true, collection)
      end
    end

    def validate_format_value(column_spec, field_value, collection)
      # Bypass some field validations
      if column_spec[:kind] == 'hierarchy'
        raise "Hierarchy fields can only be created via web in the Layers page"
      elsif column_spec[:kind] == 'select_one' || column_spec[:kind] == 'select_many'
        # options will be created
        return field_value
      end

      column_header = column_spec[:code]? column_spec[:code] : column_spec[:label]

      sample_field = Field.new kind: column_spec[:kind], code: column_header

      # We need the collection to validate site_fields
      sample_field.collection = collection

      sample_field.apply_format_and_validate(field_value, true, collection)
    end

    def to_columns(collection, rows, admin)
      fields = collection.fields.index_by &:code
      columns_initial_guess = []
      rows[0].each do |header|
        column_spec = {}
        column_spec[:header] = header ? header.strip : ''
        column_spec[:kind] = :text
        column_spec[:code] = header ? header.downcase.gsub(/\s+/, '') : ''
        column_spec[:label] = header ? header.titleize : ''
        columns_initial_guess << column_spec
      end

      columns_initial_guess.each_with_index do |column, i|
        guess_column_usage(column, fields, rows, i, admin)
      end
    end

    def guess_column_usage(column, fields, rows, i, admin)
      if (field = fields[column[:header]])
        column[:use_as] = :existing_field
        column[:layer_id] = field.layer_id
        column[:field_id] = field.id
        column[:kind] = field.kind.to_sym
        return
      end

      if column[:header] =~ /^resmap-id$/i
        column[:use_as] = :id
        column[:kind] = :id
        return
      end

      if column[:header] =~ /^name$/i
        column[:use_as] = :name
        column[:kind] = :name
        return
      end

      if column[:header] =~ /^\s*lat/i
        column[:use_as] = :lat
        column[:kind] = :location
        return
      end

      if column[:header] =~ /^\s*(lon|lng)/i
        column[:use_as] = :lng
        column[:kind] = :location
        return
      end

      if column[:header] =~ /last updated/i
        column[:use_as] = :ignore
        column[:kind] = :ignore
        return
      end

      if not admin
        column[:use_as] = :ignore
        return
      end

      found = false

      rows[1 .. -1].each do |row|
        next if row[i].blank?

        found = true

        if row[i].start_with?('0')
          column[:use_as] = :new_field
          column[:kind] = :text
          return
        end

        begin
          Float(row[i])
        rescue
          column[:use_as] = :new_field
          column[:kind] = :text
          return
        end
      end

      if found
        column[:use_as] = :new_field
        column[:kind] = :numeric
      else
        column[:use_as] = :ignore
      end
    end

    def read_xls_for(user, collection)
      xls = CSV.read(file_for(user, collection))

      # Remove empty rows at the end
      while (last = csv.last) && last.empty?
        csv.pop
      end

      csv
    end

    def file_for(user, collection)
      "#{TmpDir}/#{user.id}_#{collection.id}.xls"
    end
  end

  def self.generate_error_description_list(hierarchy_csv)
    hierarchy_errors = []
    hierarchy_csv.each do |item|
      message = ""
      if item[:error]
        message << "Error: #{item[:error]}"
        message << " " + item[:error_description] if item[:error_description]
        message << " in line #{item[:order]}." if item[:order]
      end

      hierarchy_errors << message if !message.blank?
    end
    hierarchy_errors.join("<br/>").to_s
  end

  def self.get_file_path(user, collection)
    "#{TmpDir}/#{user.id}_#{collection.id}.xls"
  end

  def self.validate_existing(csv_string)
    i = 0
    items = {}
    csv = CSV.parse(csv_string)
    csv.each do |row|
      item = {}
      if row[0] == 'Code'
        next
      else
        i = i+1
        item[:order] = i

        if row.length != 3
          item[:error] = "Wrong format."
          item[:error_description] = "Invalid column number"
        else

          #Check unique name
          name = row[2].strip
          
          #Check unique id
          id = row[0].strip
          if items.any?{|item| item.second[:id] == id}
            item[:error] = "Invalid id."
            item[:error_description] = "Hierarchy id should be unique"
            error = true
          else
            if Place.find_by_code id
              item[:error] = "Invalid id."
              item[:error_description] = "Hierarchy id should be unique"
              error = true
            end
          end

          #Check parent id exists
          parent_id = row[1]
          if(parent_id.present? && !csv.any?{|csv_row| csv_row[0].strip == parent_id.strip})
            item[:error] = "Invalid parent value."
            item[:error_description] = "ParentID should match one of the Hierarchy ids"
            error = true
          end

          if !error
            item[:id] = id
            item[:parent] = row[1].strip if row[1].present?
            item[:name] = name
          end
        end

        items[item[:order]] = item
      end
    end
    items
  end
end
