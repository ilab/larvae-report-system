class HierarchyMembership < ActiveRecord::Base

  belongs_to :user
  belongs_to :place

  def self.remove_memberships(place)
  	user_list = HierarchyMembership.find_all_by_place_id(place.id).map(&:user_id)
  	user_list.each do |id|
  		Membership.where("user_id=? and collection_id=?",id, place.collection_id).destroy_all
  	end
  end

end