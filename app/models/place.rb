# == Schema Information
#
# Table name: places
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  code       :string(255)
#  kind_of    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  ancestry   :string(255)
#
# Indexes
#
#  index_places_on_ancestry  (ancestry)
#
require 'csv'

class Place < ActiveRecord::Base
  has_ancestry(orphan_strategy: :destroy)
  belongs_to :collection

  validates :name, :code, presence: true
  validates :code, uniqueness: true

  PLACE_TYPE_NAT = 'NATIONAL'
  PLACE_TYPE_PHD = 'PHD'
  PLACE_TYPE_OD  = 'OD'
  PLACE_TYPE_HC  = 'HC'

  before_save :set_my_type

  has_many :hierarchy_memberships

  after_destroy :delete_members

  def set_my_type
    self.kind_of = my_type
  end

  def my_type
    if self.parent.present?
      if self.parent.is_kind_of_national? 
        Place::PLACE_TYPE_PHD
      elsif self.parent.is_kind_of_phd? 
        Place::PLACE_TYPE_OD
      elsif self.parent.is_kind_of_od? 
        Place::PLACE_TYPE_HC
      end
    else
      Place::PLACE_TYPE_NAT
    end
  end

  def is_kind_of_national?
    self.kind_of == Place::PLACE_TYPE_NAT
  end

  def is_kind_of_phd?
    self.kind_of == Place::PLACE_TYPE_PHD
  end

  def is_kind_of_od?
    self.kind_of == Place::PLACE_TYPE_OD
  end

  def is_kind_of_hc?
    self.kind_of == Place::PLACE_TYPE_HC
  end


  def od
    return nil if is_kind_of_phd?
    return self if is_kind_of_od?
    return self.parent if is_kind_of_hc?
  end

  def phd
    return self if is_kind_of_phd?
    return self.parent if is_kind_of_od?
    return self.parent.parent if is_kind_of_hc?
  end

  def hc
    return self if is_kind_of_hc?
    nil
  end

  def nat
    return self if is_kind_of_national?
    return self.parent if is_kind_of_phd?
    return self.parent.parent if is_kind_of_od?
    return self.parent.parent.parent if is_kind_of_hc?
  end

  def self.phds
    where([ "kind_of = ? AND ancestry is NULL ", Place::PLACE_TYPE_PHD ] )
  end

  def self.phds_list
    self.phds.pluck(:name, :id)
  end

  def self.ods_list(phd_id)
    return [] unless phd_id.present?
    ods = where(["kind_of = ?", Place::PLACE_TYPE_OD])
    ods = ods.where(["ancestry = ?", phd_id])
    ods.pluck(:name, :id)
  end

  def self.decode_hierarchy_csv(string)
    csv = CSV.parse(string)
    items = validate_format(csv)
    items.each do |order, item|
      if item[:parent].present? && !item[:error].present?
        parent_candidates = items.select{|key, hash| hash[:id] == item[:parent]}

        if parent_candidates.any?
          parent = parent_candidates.first[1]
        end

        if parent
          parent[:sub] ||= []
          parent[:sub] << item
        end
      end
    end
    items = items.reject do |order, item|
      if item[:parent] && !item[:error].present?
        item.delete :parent
        true
      else
        false
      end
    end
    items.values
    rescue Exception => ex
      return [{error: ex.message}]
  end

  def self.generate_error_description_location(locations_csv)
    locations_errors = []
    locations_csv.each do |item|
      message = ""

      if item[:error]
        message << "Error: #{item[:error]}"
        message << " " + item[:error_description] if item[:error_description]
        message << " in line #{item[:order]}." if item[:order]
      end

      locations_errors << message if !message.blank?
    end
    locations_errors.join("<br/>").to_s
  end

  def self.generate_error_description_list(hierarchy_csv)
    hierarchy_errors = []
    hierarchy_csv.each do |item|
      message = ""
      if item[:error]
        message << "Error: #{item[:error]}"
        message << " " + item[:error_description] if item[:error_description]
        message << " in line #{item[:order]}." if item[:order]
      end

      hierarchy_errors << message if !message.blank?
    end
    hierarchy_errors.join("<br/>").to_s
  end

  def self.generate_ancestry(id)
    parent = Place.find_by_id id
    if parent 
      if parent.ancestry
        return parent.ancestry + "/" +parent.id.to_s 
      else
        return parent.id.to_s
      end
    else
      nil
    end
  end

  def self.to_hierachy(collection)
    places = collection.places.pluck(:id, :code , :ancestry, :name, :kind_of)
    to_hierarchy_format places
  end

  def self.validate_format(csv)
    i = 0
    items = {}
    csv.each do |row|
      item = {}
      if row[0] == 'ID'
        next
      else
        i = i+1
        item[:order] = i

        if row.length != 5
          item[:error] = "Wrong format."
          item[:error_description] = "Invalid column number"
        else
          name = row[3].strip
          id = row[0].to_s.strip
          if items.any?{|item| item.second[:id] == id}
            item[:error] = "Invalid id."
            item[:error_description] = "Hierarchy id should be unique"
            error = true
          end

          if !error
            item[:id] = id
            item[:parent] = row[2].split("/").last.strip if row[2].present?
            item[:name] = name
            item[:code] = row[1]
            item[:kind_of] = row[4]
          end
        end

        items[item[:order]] = item
      end
    end
    items
  end

  def delete_members
    HierarchyMembership.remove_memberships(self)
    self.hierarchy_memberships.destroy_all
  end

  def get_hierarchy_view_format 
    if self.subtree.present?
      subtree = self.subtree.pluck(:id, :code , :ancestry, :name, :kind_of)
      Place.to_hierarchy_format(subtree, true) 
    else
      return self
    end
  end

  def self.to_hierarchy_format subtree, no_parent = false
    # places = CSV.parse(string)

    # First read all items into a hash
    # And validate it's content
    items = validate_format(subtree)

    if no_parent
      items[1][:parent] = nil
    end

    # Add to parents
    items.each do |order, item|
      if item[:parent].present? && !item[:error].present?
        parent_candidates = items.select{|key, hash| hash[:id] == item[:parent]}

        if parent_candidates.any?
          parent = parent_candidates.first[1]
        end

        if parent
          parent[:sub] ||= []
          parent[:sub] << item
        end
      end
    end

    # Remove those that have parents, and at the same time delete their parent key
    items = items.reject do |order, item|
      if item[:parent] && !item[:error].present?
        item.delete :parent
        true
      else
        false
      end
    end


    items.values

    rescue Exception => ex
      return [{error: ex.message}]
  end

  def get_sites(user, from_week, to_week)
    calculation_fields = collection.fields.find_all_by_kind("calculation")
    search = collection.new_search({:current_user => user})

    search.between_week(from_week, to_week)
    search.place_in(self.subtree_ids)
    search.unlimited
    results = search.ui_results
    tmp = {}
    results.each do |r|
      tmp = tmp.merge(r['_source']["properties"]) {|key,val1,val2| val1.to_i + val2.to_i}
    end
    calculation_fields.each do |field|
      field.config["dependent_fields"].each do |key, df|
        field.config["code_calculation"].gsub!("${#{df['code']}}", '"' + tmp["#{df['id']}"].to_s + '"' + ".to_f" )
      end
      tmp[field.id.to_s] = eval(field.config["code_calculation"]).round(2)
      tmp[field.id.to_s] = 0 if tmp[field.id.to_s].nan?
    end
    return {"#{id.to_s}" => tmp}
  end

  def get_site_by_date(user, from_date, to_date)
    calculation_fields = collection.fields.find_all_by_kind("calculation")
    search = collection.new_search({:current_user => user})

    search.created_between(from_date, to_date)
    search.place_in(self.subtree_ids)
    search.unlimited
    results = search.ui_results
    tmp = {}
    results.each do |r|
      tmp = tmp.merge(r['_source']["properties"]) {|key,val1,val2| val1.to_i + val2.to_i}
    end

    calculation_fields.each do |field|
      field.config["dependent_fields"].each do |key, df|
        field.config["code_calculation"].gsub!("${#{df['code']}}", '"' + tmp["#{df['id']}"].to_s + '"' + ".to_f")
      end
      tmp[field.id.to_s] = eval(field.config["code_calculation"]).round(2)
      tmp[field.id.to_s] = 0 if tmp[field.id.to_s].nan?
    end

    return tmp;
  end
end
