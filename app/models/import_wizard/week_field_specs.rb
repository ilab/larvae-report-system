class ImportWizard::WeekFieldSpecs < ImportWizard::BaseFieldSpecs
  def process(row, site)
    site.week = row[@column_spec[:index]]
  end
end
