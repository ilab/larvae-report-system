class ImportWizard::YearFieldSpecs < ImportWizard::BaseFieldSpecs
  def process(row, site)
    site.year = row[@column_spec[:index]]
  end
end
