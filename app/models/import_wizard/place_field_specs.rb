class ImportWizard::PlaceFieldSpecs < ImportWizard::BaseFieldSpecs
  def process(row, site)
  	place = site.collection.places.find_by_code(row[@column_spec[:index]])
  	if place
    	site.place_id = place.id 
    else
    	site.place_id = nil
    end
  end
end
